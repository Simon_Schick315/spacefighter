
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"

GameplayScreen::GameplayScreen(const int levelIndex)
{
	m_pLevel = nullptr; // sets the level pointer that it inherits from the header file to null
	switch (levelIndex) // switch statement is used to check what the value the Level class is
	{
	case 0: m_pLevel = new Level01(); break; // calls the level's header file and creates a new level object associated with the case 
	} // we can add more levels in the future by adding in more cases and assigning header files to them


	SetTransitionInTime(1.0f); // calls the function in the Screen.h file to create a fade-in effect. Can send in a float to set how long the effect goes on
	SetTransitionOutTime(0.5f); // acts similarly to the previous function, but fades out instead

	Show(); // calls the Show function from Screen.cpp
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}
